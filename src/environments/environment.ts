// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyBDZfHjrkvg4dH44jWmcOO-3vEO38eQO2E",
    authDomain: "exam-isia.firebaseapp.com",
    projectId: "exam-isia",
    storageBucket: "exam-isia.appspot.com",
    messagingSenderId: "864282363906",
    appId: "1:864282363906:web:136f7a5e00a96255e63f1b"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
