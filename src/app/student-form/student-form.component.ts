import { Student } from './../interfaces/student';
import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'student-form',
  templateUrl: './student-form.component.html',
  styleUrls: ['./student-form.component.css']
})
export class StudentFormComponent implements OnInit {
  student:string;
  
    onSubmit(){
      this.router.navigate(['/students',this.student]); 
    }
  
    constructor(private router:Router) { }
  
    ngOnInit(): void {
    }
  
  } 
